﻿using UnityEngine;
using System.Collections;

public class NumberWizard : MonoBehaviour
{
		int max;
		int min;
		int guess;
		// Use this for initialization
		void Start ()
		{
				StartGame ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.UpArrow)) {
						min = guess;
						CalculateGuess ();
				} else if (Input.GetKeyDown (KeyCode.DownArrow)) {
						max = guess;
						CalculateGuess ();
				} else if (Input.GetKeyDown (KeyCode.KeypadEnter)) {
						print ("I won!");		
				}
		}

		private void  CalculateGuess ()
		{
				guess = (max + min) / 2;
				print ("Higher or lower than " + guess);
		}

		public void StartGame ()
		{
				max = 1000;
				min = 1;
				guess = 500;

				print ("=======================================");
				print ("Welcome to the game");
				print ("Think of a number without telling me");
		
				print ("The highest number you can pick is: " + max);
				print ("The lowest number you can pick is: " + min);
		
				print ("Is the number higher than " + guess);
				print ("Up = higher, down = lower, return = equal");

				max = max + 1;
		}

}
